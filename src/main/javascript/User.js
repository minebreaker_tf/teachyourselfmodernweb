import React from "react"
import environment from "./relay-environment"
import {graphql, QueryRenderer} from "react-relay"

export default class User extends React.Component {
    render() {
        return (
            <QueryRenderer
                environment={environment}
                query={graphql`
                    query UserQuery {
                        user {
                            id
                            name
                        }
                    }
                `}
                variables={{}}
                render={({ props, error }) => {
                    return error ? <p>Error!</p> :
                        props ? <p>Id: {props.user.id}, Name: {props.user.name}</p>
                            : <p>Loading</p>
                }} />
        )
    }
}
