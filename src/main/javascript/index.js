import React from "react"
import main from "./main"

(function (window) {
    if (document.readyState !== "loading") {
        main(window)
    } else {
        document.addEventListener("DOMContentLoaded", main)
    }
})(window)
