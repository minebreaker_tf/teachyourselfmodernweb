import React from "react"
import ReactDOM from "react-dom"
import User from "./user"

function App() {
    return <User type="common" />
}

export default function (window) {

    const document = window.document

    ReactDOM.render(
        <App name="world" />,
        document.getElementById("app")
    )
}
