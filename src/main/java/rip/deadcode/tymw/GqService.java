package rip.deadcode.tymw;

import graphql.ExecutionInput;
import graphql.ExecutionResult;
import graphql.GraphQL;
import graphql.schema.StaticDataFetcher;
import graphql.schema.idl.RuntimeWiring;
import graphql.schema.idl.SchemaGenerator;
import graphql.schema.idl.SchemaParser;
import org.springframework.stereotype.Service;

import java.io.InputStreamReader;
import java.util.Map;

@Service
public final class GqService {

    private final GraphQL gq;

    private static class User {
        private String id;
        private String name;

        private User( String id, String name ) {
            this.id = id;
            this.name = name;
        }
    }

    public GqService() {

        var schemaString = new InputStreamReader( ClassLoader.getSystemResourceAsStream( "schema.graphql" ) );
        var typeDefinitions = new SchemaParser().parse( schemaString );
        var wiring = RuntimeWiring
                .newRuntimeWiring()
                .type( "Query", w -> w.dataFetcher( "user", new StaticDataFetcher( Map.of( "id", "123", "name", "John" ) ) ) )
                .build();
        var schema = new SchemaGenerator().makeExecutableSchema( typeDefinitions, wiring );

        gq = GraphQL.newGraphQL( schema ).build();
    }

    public ExecutionResult query( String query, Map<String, Object> variables ) {
        return gq.execute( ExecutionInput.newExecutionInput()
                                         .query( query )
                                         .variables( variables )
                                         .build() );
    }
}
