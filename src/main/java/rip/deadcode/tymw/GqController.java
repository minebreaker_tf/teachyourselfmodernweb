package rip.deadcode.tymw;

import com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Map;

@Controller
public class GqController {

    private final GqService service;

    @Autowired
    public GqController( GqService service ) {
        this.service = service;
    }

    @PostMapping(
            value = "/q",
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @ResponseBody
    public Map handle( @RequestBody Map<String, Object> request ) throws JsonProcessingException {
        String query = (String) request.get( "query" );
        //noinspection unchecked
        Map<String, Object> variables = (Map<String, Object>) request.get( "variables" );

        var result = service.query( query, variables );
        if ( result.getErrors().isEmpty() ) {
            return Map.of( "data", result.getData() );
        } else {
            return Map.of( "errors", result.getErrors() );
        }
    }
}
