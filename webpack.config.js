const path = require("path")

module.exports = {
    mode: "development",
    entry: "./src/main/javascript/index.js",
    output: {
        filename: "index.js",
        path: path.join(__dirname, "src/main/resources/public")
    },
    module: {
        rules: [
            {
                use: {
                    loader: "babel-loader"
                }
            }
        ]
    }
}
